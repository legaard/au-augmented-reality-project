package com.jojuice.ar.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jojuice.milestones.Milestone1;
import com.jojuice.milestones.Milestone2;
import com.jojuice.milestones.Milestone3;
import com.jojuice.project.FinalProject;
import org.opencv.core.Core;

public class DesktopLauncher {
	public static void main (String[] arg) {

		try {
			int milestoneNumber = Integer.parseInt(arg[0]);

			switch (milestoneNumber) {
                case 0:
                    setupFinalProject();
                    break;
				case 1:
					setupMilestone1();
					break;
				case 2:
					setupMilestone2();
					break;
				case 3:
					setupMilestone3();
					break;
				default:
					System.out.println("Milestone number not supported (yet). Please add a valid number");
					System.exit(-1);
			}

		} catch (ArrayIndexOutOfBoundsException e){
			System.out.println("Please provide a milestone number as the first argument");
			System.exit(-1);
		}

	}

	private static void setupMilestone1() {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new Milestone1(), config);
	}

	private static void setupMilestone2() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 640;
		config.height = 480;
		config.depth = 32;
		new LwjglApplication(new Milestone2(), config);
	}

	private static void setupMilestone3() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 640;
		config.height = 480;
		config.depth = 32;
		new LwjglApplication(new Milestone3(), config);
	}

    private static void setupFinalProject() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 640;
        config.height = 480;
        config.depth = 32;
        new LwjglApplication(new FinalProject(), config);
    }
}
