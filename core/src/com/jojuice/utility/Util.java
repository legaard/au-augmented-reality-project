package com.jojuice.utility;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Lorentzen on 03/03/16.
 */
public class Util {

    public static void setupEnvironment(Environment environment, Vector3 startPosition, Camera camera) {
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.5f, 0.5f, 0.5f, 1f));
        DirectionalLight directionalLight = new DirectionalLight();

        Vector3 direction = new Vector3(startPosition.x - camera.position.x,
                startPosition.y - camera.position.y,
                startPosition.z - camera.position.z);
        direction = direction.nor();
        directionalLight.set(0.8f, 0.8f, 0.8f, direction.x, direction.y, direction.z);
        environment.add(directionalLight);
    }

    public static void setupCamera(Camera camera, int cameraWidth, int cameraHeight) {
        camera.viewportWidth = cameraWidth;
        camera.viewportHeight = cameraHeight;
        camera.position.set(4f, 4f, 4f);
        camera.lookAt(new Vector3(0, 0, 0));
        camera.up.set(0, 1, 0);
        camera.near = 0.001f;
        camera.far = 300f;
        camera.update();
    }

}
