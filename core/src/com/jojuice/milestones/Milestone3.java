package com.jojuice.milestones;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.jojuice.utility.PerspectiveOffCenterCamera;
import com.jojuice.utility.UtilAR;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgproc.*;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.util.ArrayList;
import java.util.List;

public class Milestone3 extends ApplicationAdapter {

    private PerspectiveOffCenterCamera perspectiveCam;
    private ModelBatch modelBatch;
    private ModelBuilder modelBuilder;

    private VideoCapture cap;
    private MatOfPoint2f camEye;

    private static int CAMERA_WIDTH = 640;
    private static int CAMERA_HEIGHT = 480;

    private MatOfPoint2f squareImgCoords;
    private MatOfPoint3f squareObjCoords;
    private MatOfPoint2f homoObjCoords = new MatOfPoint2f();

    private Mat intrinsics;
    private MatOfDouble distortion;
    private Mat warpedImg = new Mat();

    @Override
    public void create() {
        modelBatch = new ModelBatch();
        modelBuilder = new ModelBuilder();

        setupCamera();

        // Setup video-capture
        cap =  new VideoCapture(0);
        cap.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, CAMERA_WIDTH);
        cap.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, CAMERA_HEIGHT);

        // Setup matrices for camera image, image coordinates and object coordinates
        camEye = new MatOfPoint2f();
        squareImgCoords = new MatOfPoint2f();
        squareObjCoords = new MatOfPoint3f();

        // Create square object coordinates
        List<Point3> objCoords = new ArrayList<>();
        objCoords.add(new Point3(0, 0, 0));
        objCoords.add(new Point3(1, 0, 0));
        objCoords.add(new Point3(1, 0, 1));
        objCoords.add(new Point3(0, 0, 1));
        squareObjCoords.fromList(objCoords);

        // create homography object coordinates (640 X 640 image)
        homoObjCoords.alloc(4);
        homoObjCoords.put(0, 0, 0, 0);
        homoObjCoords.put(1, 0, CAMERA_WIDTH, 0);
        homoObjCoords.put(2, 0, CAMERA_WIDTH, CAMERA_WIDTH);
        homoObjCoords.put(3, 0, 0, CAMERA_WIDTH);

        intrinsics = UtilAR.getDefaultIntrinsics(CAMERA_WIDTH, CAMERA_HEIGHT);
        distortion = UtilAR.getDefaultDistortionCoefficients();
        perspectiveCam.setByIntrinsics(intrinsics, CAMERA_WIDTH, CAMERA_HEIGHT);
    }

    @Override
    public void render() {
        clearViewport();

        if (cap.read(camEye)) {
            // Make image binary
            Mat binaryCamEye = Mat.eye(128, 128, CvType.CV_8UC1);
            Imgproc.cvtColor(camEye, binaryCamEye, Imgproc.COLOR_RGB2GRAY);
            Imgproc.threshold(binaryCamEye, binaryCamEye, 120, 255, Imgproc.THRESH_BINARY); // Threshold 80, light sensitive

            // Find contours from the binary image
            List<MatOfPoint> contours = new ArrayList<>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(binaryCamEye, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

            // Find polygons
            List<MatOfPoint2f> squares = new ArrayList<>();
            List<MatOfPoint> contoursToDraw = new ArrayList<>();

            for (MatOfPoint contour : contours) {
                MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());
                MatOfPoint2f polygon = new MatOfPoint2f();

                // Approximate polygon and save to polygon
                Imgproc.approxPolyDP(contour2f, polygon, 8, true);

                // Create polygon contour
                MatOfPoint polygonCvt = new MatOfPoint(polygon.toArray());

                // Filter polygons: Check if polygon has at least 4 points, the area is larger than 1500 and rectangle
                // is black and that the contour is convex (ignore wrong weird shapes)
                if (polygonCvt.size().height == 4 && Imgproc.contourArea(polygon, true) > 3000
                        && Imgproc.isContourConvex(polygonCvt)) {
                    contoursToDraw.add(polygonCvt);
                    squares.add(polygon); // polygon consists of 4 (x,y) points = image coordinates
                }
            }

            Imgproc.drawContours(camEye, contoursToDraw, -1, new Scalar(255, 0, 0)); // 5th param = thickness, -1 to fill
            UtilAR.imDrawBackground(camEye);

            for (MatOfPoint2f square : squares) {
                Mat rVec = new Mat();
                Mat tVec = new Mat();

                Calib3d.solvePnP(squareObjCoords, square, intrinsics, distortion, rVec, tVec);
                UtilAR.setCameraByRT(rVec, tVec, perspectiveCam);

                ModelInstance box = new ModelInstance(modelBuilder.createBox(0.5f, 0.5f, 0.5f,
                        new Material(ColorAttribute.createDiffuse(Color.FOREST)),
                        VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal));

                modelBatch.begin(perspectiveCam);

                box.transform.translate(0.5f, 0.25f, 0.5f);

                modelBatch.render(box);
                modelBatch.end();

                Mat homoGraphy = Calib3d.findHomography(square, homoObjCoords);

                Imgproc.warpPerspective(camEye, warpedImg, homoGraphy, new Size(CAMERA_WIDTH, CAMERA_WIDTH));

                UtilAR.imShow(warpedImg);
            }
        }

    }

    @Override
    public void resize(int width, int height) {
        perspectiveCam.viewportWidth = width;
        perspectiveCam.viewportHeight = height;
        perspectiveCam.update();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        cap.release();
        modelBatch.dispose();
    }

    private void clearViewport() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    private void setupCamera() {
        perspectiveCam = new PerspectiveOffCenterCamera();
        perspectiveCam.viewportWidth = CAMERA_WIDTH;
        perspectiveCam.viewportHeight = CAMERA_HEIGHT;
        perspectiveCam.position.set(4f, 4f, 4f);
        perspectiveCam.lookAt(new Vector3(0, 0, 0));
        perspectiveCam.up.set(0, 1, 0);
        perspectiveCam.near = 0.001f;
        perspectiveCam.far = 300f;
        perspectiveCam.update();
    }
}
