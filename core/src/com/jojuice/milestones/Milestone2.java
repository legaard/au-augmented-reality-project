package com.jojuice.milestones;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.jojuice.utility.PerspectiveOffCenterCamera;
import com.jojuice.utility.UtilAR;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.util.ArrayList;

public class Milestone2 extends ApplicationAdapter {

    private PerspectiveOffCenterCamera perspectiveCam;
    private ModelBatch modelBatch;
    private ModelBuilder modelBuilder;
    private ModelInstance[][] cubes;

    private Vector3 startPosition;
    private Environment environment;

    private VideoCapture cap;
    private MatOfPoint2f camEye;

    private static int CAMERA_WIDTH = 640;
    private static int CAMERA_HEIGHT = 480;

    private Size boardSize = new Size(9, 6);
    private double numberOfCoords = boardSize.width * boardSize.height;
    private double width = 4;   // Number of cubes on each row
    private double height = 5;  // Number of rows

    private MatOfPoint2f imgCoords;
    private MatOfPoint3f objCoords;

    private Mat intrinsics;
    private MatOfDouble distortion;

    private int rotationCounter = 0;

    @Override
    public void create() {
        // Set the start position to be in the corner of a cube
        startPosition = new Vector3(0.5f, 0.5f, 0.5f);

        modelBatch = new ModelBatch();
        modelBuilder = new ModelBuilder();

        cubes = new ModelInstance[(int) width][(int) height];

        setupCamera();
        setupEnvironment();
        setupCubes();

        // Setup video-capture
        cap =  new VideoCapture(0);
        cap.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, CAMERA_WIDTH);
        cap.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, CAMERA_HEIGHT);

        // Setup matrices for camera image, image coordinates and object coordinates
        camEye = new MatOfPoint2f();
        imgCoords = new MatOfPoint2f();
        objCoords = new MatOfPoint3f();
        objCoords.alloc((int) numberOfCoords);

        intrinsics = UtilAR.getDefaultIntrinsics(CAMERA_WIDTH, CAMERA_HEIGHT);
        distortion = UtilAR.getDefaultDistortionCoefficients();
    }

    @Override
    public void render() {
        clearViewport();

        boolean isBoardFound = false;

        try {
            // Read camera image into camEye matrix and render the image as background
            if(cap.read(camEye)) {
                UtilAR.imDrawBackground(camEye);

                // Convert camera image to grayscale and find chessboard corners
                Imgproc.cvtColor(camEye, camEye, Imgproc.COLOR_RGB2GRAY);
                isBoardFound = Calib3d.findChessboardCorners(camEye, boardSize, imgCoords,
                        Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE + Calib3d.CALIB_CB_FAST_CHECK);
            }

            // If chessboard is found create object coordinates and use solvePnP to find rotation and translation
            if (isBoardFound) {

                // Create the object coordinate system
                ArrayList<Point3> point3List = new ArrayList<>();
                for (int i = 0; i < imgCoords.size().height; i++) {
                    double column = i % boardSize.width;            // 0 ... 8
                    double row = Math.floor(i / boardSize.width);   // 0 ... 5

                    Point3 p = new Point3(column, 0, row);
                    point3List.add(p);
                }

                //Populate matrix with Point3 objects
                objCoords.fromList(point3List);

                Mat rVec = new Mat();
                Mat tVec = new Mat();

                Calib3d.solvePnP(objCoords, imgCoords, intrinsics, distortion, rVec, tVec);

                UtilAR.setCameraByRT(rVec, tVec, perspectiveCam);
                perspectiveCam.setByIntrinsics(intrinsics, CAMERA_WIDTH, CAMERA_HEIGHT);

                // Draw graphics on the chessboard. Placing cubes on the black tiles
                rotationCounter += 3;           // Rotation speed
                double sin = rotationCounter;   // Make the scaling dependent on rotation
                sin = Math.toRadians(sin);      // Convert to radians for use by Math.sin()

                modelBatch.begin(perspectiveCam);
                for (int j = 0; j < height; j++) {
                    for (int i = 0; i < width; i++) {
                        cubes[i][j].transform.idt(); // Reset each cube every render

                        int xOffset = i * 2;            // Draw in x coordinates 0,2,4,6
                        // Avoid starting on white tile. When j = 1 & 3 offset x one tile
                        if (j % 2 == 1) xOffset += 1;   // Draw in x coordinates 1,3,5,7

                        // (3) Translate cubes to correct positions
                        Vector3 cubePosition = new Vector3(startPosition.x + xOffset, startPosition.y + (float) (Math.sin(sin) / 2), startPosition.z + j);
                        cubes[i][j].transform.translate(cubePosition);

                        // (2) Scale cubes in y-axis using sin-wave 0 -> 1 -> 0 -> -1 -> 0
                        cubes[i][j].transform.scale(1f, 1f + (float) Math.sin(sin), 1f);

                        // (1) Rotate cubes around y-axis
                        cubes[i][j].transform.rotate(Vector3.Y, rotationCounter);

                        modelBatch.render(cubes[i][j], environment);
                    }
                }

                modelBatch.end();
            }

        } catch (Exception e ){
            e.printStackTrace();
            System.out.println("An error occurred!");
        }


    }

    @Override
    public void dispose() {
        cap.release();
        modelBatch.dispose();
    }

    @Override
    public void resize(int width, int height) {
        perspectiveCam.viewportWidth = width;
        perspectiveCam.viewportHeight = height;
        perspectiveCam.update();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    private void setupCamera() {
        perspectiveCam = new PerspectiveOffCenterCamera();
        perspectiveCam.viewportWidth = CAMERA_WIDTH;
        perspectiveCam.viewportHeight = CAMERA_HEIGHT;
        perspectiveCam.position.set(4f, 4f, 4f);
        perspectiveCam.lookAt(startPosition);
        perspectiveCam.up.set(0, 1, 0);
        perspectiveCam.near = 0.001f;
        perspectiveCam.far = 300f;
        perspectiveCam.update();
    }

    private void setupEnvironment() {
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.5f, 0.5f, 0.5f, 1f));
        DirectionalLight directionalLight = new DirectionalLight();

        Vector3 direction = new Vector3(startPosition.x - perspectiveCam.position.x,
                startPosition.y - perspectiveCam.position.y,
                startPosition.z - perspectiveCam.position.z);
        direction = direction.nor();
        directionalLight.set(0.8f, 0.8f, 0.8f, direction.x, direction.y, direction.z);
        environment.add(directionalLight);
    }

    private void setupCubes() {
        // Create 4 x 5 blue cubes
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cubes[i][j] = createCube(1f, Color.BLUE, 0.5f, 0.5f, 0.5f);
            }
        }
    }

    private ModelInstance createCube(float size, Color color, float x, float y, float z) {
        return new ModelInstance(modelBuilder.createBox(size, size, size,
                new Material(ColorAttribute.createDiffuse(color)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal), x, y, z);
    }

    private void clearViewport() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }
}
