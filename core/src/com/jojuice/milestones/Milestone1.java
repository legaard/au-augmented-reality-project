package com.jojuice.milestones;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

public class Milestone1 extends ApplicationAdapter {
    public PerspectiveCamera cam;
    public ModelInstance sun;
    public ModelInstance moon;
    public ModelInstance earth;
    public ModelBatch modelBatch;
    public Environment environment;
    public CameraInputController camController;
    private ModelBuilder modelBuilder;
    private Array<ModelInstance> arrowList = new Array<>(3);
    private Array<ModelInstance> cubeList = new Array<>(3);

    private Vector3 zeroVector = new Vector3(0, 0, 0);
    private Vector3 cameraPosition;

    @Override
    public void create () {
        modelBatch = new ModelBatch();

        // Create ambient lighting
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1, -0.8f, -0.2f));

        // Create a camera with 67 degrees field of view. Position the camera and point it towards (0, 0, 0)
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cameraPosition = new Vector3(5, 5, 5);
        cam.position.set(cameraPosition);
        cam.lookAt(zeroVector);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        //Instantiate the model builder to be used in various methods
        modelBuilder = new ModelBuilder();

        // Instantiate cubes
        sun = createCube(1, Color.YELLOW, 0, 0, 0);
        earth = createCube(0.5f, Color.BROWN, 0, 0, 2.5f);
        moon = createCube(0.25f, Color.WHITE, 0, 0, 2.5f);

        cubeList.add(sun);
        cubeList.add(earth);
        cubeList.add(moon);

        float arrowSize = 8;
        ModelInstance xAxisArrow = createArrow(new Vector3(-arrowSize, 0, 0), new Vector3(arrowSize, 0, 0), Color.BLUE);
        ModelInstance yAxisArrow = createArrow(new Vector3(0, -arrowSize, 0), new Vector3(0, arrowSize, 0), Color.RED);
        ModelInstance zAxisArrow = createArrow(new Vector3(0, 0, -arrowSize), new Vector3(0, 0, arrowSize), Color.GREEN);
        arrowList.add(xAxisArrow);
        arrowList.add(yAxisArrow);
        arrowList.add(zAxisArrow);

        // Enable camera control
        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(camController);
    }

    private float degreeCounter = 0;
    private float speed = 4;

    @Override
    public void render () {

        // Make the sun rotate around the y-axis
        sun.transform.rotate(Vector3.Y, 1);

        // Make the earth orbit (0,0,0) with a radius of 2.5
        orbit(earth, zeroVector, 1f, 2.5f);

        // Extract earth coordinates and make the moon orbit earth axis-aligned
        Vector3 earthPosition = extractInstancePosition(earth);

        moon.transform.rotate(Vector3.Y, degreeCounter);
        orbit(moon, earthPosition, speed, 1f);
        degreeCounter += speed;
        moon.transform.rotate(Vector3.Y, -degreeCounter);

        Vector3 moonPosition = extractInstancePosition(moon);

        // Press and hold 1 to focus on earth, 2 to focus on moon, 3 to follow earth and 4 to reset the camera
        if (Gdx.input.isKeyPressed(Input.Keys.NUM_1)) {
            cam.up.set(0, 1, 0);
            cam.lookAt(earthPosition);
            cam.update();
        } else if (Gdx.input.isKeyPressed(Input.Keys.NUM_2)){
            cam.up.set(0, 1, 0);
            cam.lookAt(moonPosition);
            cam.update();
        } else if (Gdx.input.isKeyPressed(Input.Keys.NUM_3)) {
            cam.up.set(0, 1, 0);
            cam.position.set(earthPosition);
            cam.translate(3, 3, 3);
            cam.lookAt(earthPosition);
            cam.update();
        } else if (Gdx.input.isKeyPressed(Input.Keys.NUM_4)) {
            cam.up.set(0, 1, 0);
            cam.position.set(cameraPosition);
            cam.lookAt(zeroVector);
            cam.update();
        } else {
            cam.up.set(0, 1, 0);
            camController.update();
            cam.lookAt(zeroVector);
            cam.update();
        }

        // Clear the viewport
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // Render the camera, sun and lighting
        modelBatch.begin(cam);
        modelBatch.render(cubeList, environment);
        modelBatch.render(arrowList, environment);
        modelBatch.end();
    }

    @Override
    public void dispose() {
        modelBatch.dispose();
        for (ModelInstance arrow : arrowList) {
            arrow.model.dispose();
        }
        for (ModelInstance cube : cubeList) {
            cube.model.dispose();
        }
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void resize(int width, int height) {
        cam.viewportWidth = Gdx.graphics.getWidth();
        cam.viewportHeight = Gdx.graphics.getHeight();
        cam.update();
    }

    @Override
    public void pause() {
        super.pause();
    }

    private Vector3 extractInstancePosition(ModelInstance instance) {
        float[] positionMatrix = new float[12];
        instance.transform.extract4x3Matrix(positionMatrix);
        float x = positionMatrix[9];
        float y = positionMatrix[10];
        float z = positionMatrix[11];
        return new Vector3(x, y, z);
    }

    private void orbit(ModelInstance instance, Vector3 orbitPosition, float rotationSpeed, float radius) {
        instance.transform.setTranslation(orbitPosition).rotate(Vector3.Y, rotationSpeed).translate(radius, 0, 0);
    }

    private ModelInstance createArrow(Vector3 startVector, Vector3 endVector, Color color) {
        return new ModelInstance(modelBuilder.createArrow(startVector, endVector,
                new Material(ColorAttribute.createDiffuse(color)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal));
    }

    private ModelInstance createCube(float size, Color color, float x, float y, float z) {
        return new ModelInstance(modelBuilder.createBox(size, size, size,
                new Material(ColorAttribute.createDiffuse(color)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal), x, y, z);
    }
}
