package com.jojuice.project;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.jojuice.utility.Util;
import com.jojuice.utility.UtilAR;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class FinalProject extends ApplicationAdapter {

    private Camera perspectiveCam;
    private ModelBatch modelBatch;
    private ModelBuilder modelBuilder;
    private int currentBubbleKey;
    private HashMap<Integer, Bubble> bubbleHashMap = new HashMap<>();

    private VideoCapture cap;
    private MatOfPoint2f camEye;

    private static final int CAMERA_WIDTH = 640;
    private static final int CAMERA_HEIGHT = 480;
    private static final int BINARY_THRESHOLD = 120;
    private Point viewPortCenter = new Point(CAMERA_WIDTH/2, CAMERA_HEIGHT/2);
    private int SCORE = 0;
    private Sound blop;

    private MatOfPoint3f markerObjCoords;
    private MatOfPoint2f homoObjCoords = new MatOfPoint2f();

    private Mat intrinsics;
    private MatOfDouble distortion;

    private Mat warpedImg = new Mat();
    private Vector3 startPosition;
    private Environment environment;

    // FOR DEBUGGING PURPOSES
    private final List<MatOfPoint> contoursToDraw = new ArrayList<>();
    private final List<MatOfPoint> innerContoursToDraw = new ArrayList<>();

    @Override
    public void create() {
        modelBatch = new ModelBatch();
        modelBuilder = new ModelBuilder();

        startPosition = new Vector3(0, 0, 0);

        perspectiveCam = new PerspectiveCamera(40, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        perspectiveCam.near = 0.001f;
        perspectiveCam.far = 300f;

        environment = new Environment();
        Util.setupEnvironment(environment, startPosition, perspectiveCam);

        for (int i = 1; i <= 4; i++) {
            Texture texture = new Texture(Gdx.files.internal("bubble" + 7 + ".jpg"));
            Bubble bubble = new Bubble(modelBuilder, 0.5f, 0.5f, 0.5f, texture);
            bubbleHashMap.put(i, bubble);
        }

        blop = Gdx.audio.newSound(Gdx.files.internal("blop.mp3"));

        // Generate the first random bubble
        currentBubbleKey = getRandomBubbleKey();

        // TODO Create SoundManager to play sounds for bubble pop and gun shot etc.

        // Setup video-capture
        cap =  new VideoCapture(0);
        cap.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, CAMERA_WIDTH);
        cap.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, CAMERA_HEIGHT);

        // Setup matrices for camera image, image coordinates and object coordinates
        camEye = new MatOfPoint2f();
        markerObjCoords = new MatOfPoint3f();

        // Create square object coordinates
        List<Point3> objCoords = new ArrayList<>();
        objCoords.add(new Point3(0, 0, 0));
        objCoords.add(new Point3(1, 0, 0));
        objCoords.add(new Point3(1, 0, 1));
        objCoords.add(new Point3(0, 0, 1));
        markerObjCoords.fromList(objCoords);

        // create homography object coordinates (640 X 640 image)
        homoObjCoords.alloc(4);
        homoObjCoords.put(0, 0, 0, 0);
        homoObjCoords.put(1, 0, CAMERA_WIDTH, 0);
        homoObjCoords.put(2, 0, CAMERA_WIDTH, CAMERA_WIDTH);
        homoObjCoords.put(3, 0, 0, CAMERA_WIDTH);

        intrinsics = UtilAR.getDefaultIntrinsics(CAMERA_WIDTH, CAMERA_HEIGHT);
        distortion = UtilAR.getDefaultDistortionCoefficients();
        //perspectiveCam.setByIntrinsics(intrinsics, CAMERA_WIDTH, CAMERA_HEIGHT);
    }

    @Override
    public void render() {
        clearViewport();
        perspectiveCam.update();
        modelBatch.begin(perspectiveCam);

        //Scale up bubble
        Bubble currentBubble = bubbleHashMap.get(currentBubbleKey);
        currentBubble.scaleUp(0.025f);
        bubbleHashMap.put(currentBubbleKey,currentBubble);

        //If bubble is too big then pop bubble, reset score, and selecet new bubble
        if(bubbleHashMap.get(currentBubbleKey).getScaleFactor() > 2.5f){
            System.out.println("RESTARTING. YOUR SCORE WAS: " + SCORE);
            bubbleHashMap.get(currentBubbleKey).pop();
            SCORE = 0;
            currentBubbleKey = getRandomBubbleKey();
        }

        try {
            if (cap.read(camEye)) {
                // Make image binary
                Mat binaryCamEye = convertToBinary(camEye, BINARY_THRESHOLD, CAMERA_WIDTH, CAMERA_HEIGHT);

                // Find contours from the binary image
                List<MatOfPoint> contours = new ArrayList<>();
                Imgproc.findContours(binaryCamEye, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
                //UtilAR.imDrawBackground(binaryCamEye); //DEBUG see contours

                // Extract markers from contours
                List<MatOfPoint2f> markerImgCoords = extractMarkers(contours);

                // Drawing contours on camEye for debugging purposes mainly
                Imgproc.drawContours(camEye, contoursToDraw, -1, new Scalar(255, 0, 0)); // 5th param = thickness, -1 to fill

                // Draw gun-point and score on camEye
                int gunPointCalibration = 15; // Calibrate for size of gun-point
                Point calibratedGunPoint = new Point(viewPortCenter.x - gunPointCalibration, viewPortCenter.y + gunPointCalibration);
                Imgproc.putText(camEye, "+", calibratedGunPoint, Core.FONT_HERSHEY_PLAIN, 3.0, new Scalar(0, 0, 255));
                Imgproc.putText(camEye, "SCORE " + SCORE, new Point(25, 25), Core.FONT_HERSHEY_TRIPLEX, 0.75, new Scalar(0, 255, 0));
                UtilAR.imDrawBackground(camEye);
                contoursToDraw.clear();

                // Iterate through every found marker
                for (MatOfPoint2f marker : markerImgCoords) {
                    // Extrinsics for 3D rendering
                    Mat rVec = new Mat();
                    Mat tVec = new Mat();

                    // Solve to find rotation and translation
                    boolean success = Calib3d.solvePnP(markerObjCoords, marker, intrinsics, distortion, rVec, tVec);

                    if (success) {
                        // Find homography and rectify image
                        Mat homography = Calib3d.findHomography(marker, homoObjCoords);
                        Imgproc.warpPerspective(camEye, warpedImg, homography, new Size(CAMERA_WIDTH, CAMERA_WIDTH));

                        Mat innerBinaryImg = convertToBinary(warpedImg, BINARY_THRESHOLD, CAMERA_WIDTH, CAMERA_WIDTH);
                        List<MatOfPoint> innerContours = new ArrayList<>();
                        Imgproc.findContours(innerBinaryImg, innerContours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                        int numOfInnerSquares = 0;
                        // Iterate inner contours if any are found
                        if (!innerContours.isEmpty()) {
                            for (MatOfPoint innerContour : innerContours) {
                                MatOfPoint2f innerContour2f = new MatOfPoint2f(innerContour.toArray());
                                MatOfPoint2f innerPolygon = new MatOfPoint2f();

                                // Approximate a polygon that is closed with 8 as accuracy
                                Imgproc.approxPolyDP(innerContour2f, innerPolygon, 8, true);
                                MatOfPoint innerPolygonCvt = new MatOfPoint(innerPolygon.toArray());
                                innerContoursToDraw.add(innerPolygonCvt);

                                // Filter inner polygons. Only accept square-like polygons of certain size.
                                if (innerPolygonCvt.size().height >= 4 && Imgproc.contourArea(innerPolygonCvt, true) < -10000) {
                                    numOfInnerSquares++;
                                }
                            }

                            boolean isMarkerHit = evaluateMarkerHit(marker);
                            int rotation = calculateRotation(marker);

                            if (numOfInnerSquares == currentBubbleKey) {
                                Bubble bubble = bubbleHashMap.get(currentBubbleKey);
                                UtilAR.setTransformByRT(rVec, tVec, bubble.getModelInstance().transform);
                                renderBubble(bubble, isMarkerHit, rotation);
                            }

                        }

                        // DEBUG. See inner contours
                        //Imgproc.drawContours(warpedImg, innerContoursToDraw, -1, new Scalar(255, 0, 0));
                        //UtilAR.imShow(warpedImg);
                        //innerContoursToDraw.clear();
                    }
                }

            }
        } catch (Exception e) {
            System.out.println("Unknown error caught");
            dispose();

        }
        modelBatch.end();
    }

    private void renderBubble(Bubble bubble, boolean isMarkerHit, int rotation) {
        ModelInstance bubbleModel = bubble.getModelInstance();
        float scaleFactor = bubble.getScaleFactor();

        bubbleModel.transform.translate(0.5f, 0.25f + scaleFactor/4 , 0.5f);
        bubbleModel.transform.scale(1 + scaleFactor, 1 + scaleFactor, 1+  scaleFactor);
        bubbleModel.transform.rotate(Vector3.Y, rotation);
        modelBatch.render(bubbleModel, environment);

        // If the marker is hit and is big enough, increment score and pop bubble.
        if (isMarkerHit && Gdx.input.isKeyPressed(Input.Keys.UP)) {
            blop.play();
            System.out.println("Hit bubble");
            SCORE++;
            bubble.pop();
            bubbleHashMap.put(currentBubbleKey, bubble);
            currentBubbleKey = getRandomBubbleKey();
            System.out.println(currentBubbleKey);
        }
    }

    private List<MatOfPoint2f> extractMarkers(List<MatOfPoint> contours) {
        List<MatOfPoint2f> markerImgCoords = new ArrayList<>();
        for (MatOfPoint contour : contours) {
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());
            MatOfPoint2f polygon = new MatOfPoint2f();

            // Approximate polygon and save to polygon.
            Imgproc.approxPolyDP(contour2f, polygon, 8, true);

            // Create polygon contour
            MatOfPoint polygonCvt = new MatOfPoint(polygon.toArray());

            // Filter polygons: Check if polygon has 4 points, the area is larger than -1000 and rectangle is white.
            if (polygonCvt.size().height == 4 && Imgproc.contourArea(polygon, true) > 1600) {
                //System.out.println(Imgproc.contourArea(polygon, true));
                contoursToDraw.add(polygonCvt);
                markerImgCoords.add(polygon); // polygon consists of 4 (x,y) points = image coordinates
            }
        }
        return markerImgCoords;
    }

    private Mat convertToBinary(Mat image, int threshold, int imageWidth, int imageHeight) {
        Mat binaryImg = Mat.eye(imageWidth, imageHeight, CvType.CV_8U);

        // Convert to gray scale image
        Imgproc.cvtColor(image, binaryImg, Imgproc.COLOR_RGB2GRAY);

        // Apply binary threshold to image
        Imgproc.threshold(binaryImg, binaryImg, threshold, 255, Imgproc.THRESH_BINARY);
        //Imgproc.adaptiveThreshold(binaryImg, binaryImg, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 11, 2);

        return binaryImg;
    }

    private boolean evaluateMarkerHit(MatOfPoint2f marker) {
        // Evaluate whether the gun-point is within the marker contour
        boolean result = Imgproc.pointPolygonTest(marker, viewPortCenter, false) == 1;
        return result;
    }

    public int calculateRotation(MatOfPoint2f marker) {
        List<Point> markerPoints = marker.toList();
        Point p0 = markerPoints.get(0);
        Point p1 = markerPoints.get(1);
        Point p3 = markerPoints.get(3);

        int threshold = 30;

        if(p1.x - p0.x > threshold && p3.y - p0.y > threshold){
            return 0;
        }

        else if(p1.x - p0.x <= threshold && p3.y - p0.y < -threshold){
            return -90;
        }

        else if(p1.x - p0.x < -threshold && p3.y - p0.y <= threshold){
            return 180;
        } else {
            return 90;
        }
    }

    private Integer getRandomBubbleKey() {
        Random r = new Random();
        int randomInt = r.nextInt(4);
        int res = randomInt + 1;

        return res != currentBubbleKey ? res : getRandomBubbleKey();
    }

    private void clearViewport() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void resize(int width, int height) {
        perspectiveCam.viewportWidth = width;
        perspectiveCam.viewportHeight = height;
        perspectiveCam.update();
    }

    @Override
    public void dispose() {
        cap.release();
        modelBatch.dispose();
        blop.dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }
}
