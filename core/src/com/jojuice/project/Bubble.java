package com.jojuice.project;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

public class Bubble {

    private ModelInstance sphere;
    private float scaleFactor = 0;

    public Bubble(ModelBuilder modelBuilder, float width, float height, float depth, Texture texture) {
        sphere = new ModelInstance(modelBuilder.createSphere(width, height, depth, 24, 24,
                new Material(TextureAttribute.createDiffuse(texture)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates));
    }

    public void scaleUp(float scaleIncrement) {
        scaleFactor += scaleIncrement;
    }

    public void pop() {
        scaleFactor = 0;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public ModelInstance getModelInstance() {
        return sphere;
    }
}
